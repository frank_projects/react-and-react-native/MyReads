import React from 'react'
import './App.css'
import Search from './Search'
import MainBookScreen from './MainBookScreen'
import * as BooksAPI from './BooksAPI'
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'

class BooksApp extends React.Component {
	state = {
		books: []
	}

	changeShelf (book, shelf){
		BooksAPI.update(book, shelf).then(respond => {
			// Update the local version of books to reflect this change
			book.shelf = shelf;


			// Get array of books without the current one
			var bookArray = this.state.books.filter(
				(currBook) => (currBook.id !== book.id)
			);


			// add the new book
			bookArray.push(book);

			this.setState({
				books: bookArray
			})

		})
	}

	componentDidMount() {
		BooksAPI.getAll()
		.then((books) => {
			this.setState(() => ({
			books
			}))
		})
	}

	getShelfName(bookId){
		var bookArray = this.state.books.filter(
			(book) => (book.id === bookId)
		);

		if (bookArray.length === 0){
			return "none";
		}
		else if (bookArray.length === 1){
			return bookArray[0].shelf;
		}
		else{
			throw "Error App.js, bookArray got more than one book which means that there was some kind of issue with the book ID. The BookId input was: " + bookId + ", and the bookArray.length: " + bookArray.length;
		}

	}

	render() {
		const { books } = this.state;
		return (

				<Router>
					<div>
							<Route exact path="/search" render={() => (
								<Search
									changeShelf = {this.changeShelf.bind(this)}
									getShelfName = {this.getShelfName.bind(this)}
									/>
							)}/>

							<Route exact  path="/" render={() => (
									<MainBookScreen
										books = {books}
										changeShelf = {this.changeShelf.bind(this)}
										getShelfName = {this.getShelfName.bind(this)}/>

							)}/>


						<div className="open-search">
								<Link to="/search">Search</Link>
						</div>
					</div>
				</Router>

		)
	}
}


export default BooksApp
